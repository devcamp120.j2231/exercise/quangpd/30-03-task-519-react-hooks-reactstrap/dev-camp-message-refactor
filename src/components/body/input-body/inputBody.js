import { Button, Col, Input, Label, Row } from "reactstrap";

function InputBody({inputMessageProps, inputMessageChangeHandlerProp, outputMessageChangeHandlerProp}) {

    const onInputChangeHandler = (event) => {
        console.log(event.target.value);
        //gọi hàm của component cha (truyền vào giá trị input) để thay đổi state input message của component cha
        inputMessageChangeHandlerProp(event.target.value);
    }

    const onButtonClickHandler = () => {
        console.log("Đã bấm nút gửi thông điệp");
        outputMessageChangeHandlerProp();
    }

    return (
        <>
            <Row className='mt-3'>
                <Col sm={12} md={12} xs={12} lg={12}>
                    <Label for="inputMessage">
                        Mesage cho bạn 12 tháng tới
                    </Label>    
                </Col>
            </Row>
            <Row className='mt-3'>
                <Col sm={12} md={12} xs={12} lg={12}>
                    <Input id="inputMessage" placeholder='Nhập vào message' onChange={onInputChangeHandler} value={inputMessageProps} />
                </Col>
            </Row>
            <Row className='mt-3'>
                <Col sm={12} md={12} xs={12} lg={12}>
                    <Button color="primary" onClick={onButtonClickHandler}>Gửi thông điệp</Button>
                </Col>
            </Row>
        </>
    )
}

export default InputBody