import like from '../../../assets/images/like-yellow-icon.png'
import { Col, Row } from "reactstrap";

function OutputBody(props) {
    console.log(props);
    return (
        <>
            <Row className='mt-3'>
                <Col sm={12} md={12} xs={12} lg={12}>
                    {props.outputMessageProps.map((value, index) => {
                        return <p key={index}>{value}</p>
                    })}                    
                </Col>
            </Row>
            <Row className='mt-3'>
                <Col sm={12} md={12} xs={12} lg={12}>
                    {props.likeDisplayProps ? <img src={like} alt='like' style={{width:"100px"}}/> : null}                        
                </Col>
            </Row>
        </>
    )
}

export default OutputBody