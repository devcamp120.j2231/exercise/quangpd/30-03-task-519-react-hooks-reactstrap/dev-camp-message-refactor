import { Col, Row } from "reactstrap";
import programing from '../../../assets/images/programing.png'

function ImageHeader() {
    return(
        <Row className='mt-3'>
            <Col sm={12} md={12} xs={12} lg={12}>
                <img src={programing} alt="programing" style={{width:"500px"}}/>
            </Col>
        </Row>
    )
}

export default ImageHeader